package team.devim.octal_converter;

import java.util.Scanner;

public class Main {

/*
Восьмеричный конвертер
*/

    public static void main(String[] args) {
        Main app = new Main();
        app.getStart();
    }

    public void getStart() {
        Scanner scanner = new Scanner(System.in);
        int decimalNumber = scanner.nextInt();
        Converter oktConverter = new Converter();
        oktConverter.toOctal(decimalNumber);
        int octalNumber = scanner.nextInt();
        Converter decConverter = new Converter();
        decConverter.toDecimal(octalNumber);
    }
}
