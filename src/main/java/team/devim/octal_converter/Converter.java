package team.devim.octal_converter;

public class Converter {
    public void toOctal(int decimalNumber) {
        int i = 0;
        int oktal = 0;
        if (decimalNumber > 0) {
            System.out.println("Десятичное число: " + decimalNumber);
            while (decimalNumber != 0) {
                oktal = (int) (oktal + (decimalNumber % 8) * Math.pow(10, i++));
                decimalNumber = decimalNumber / 8;
            }
            System.out.println("равно восьмеричному числу: " + oktal);
        } else {
            System.out.println("Число должно быть больше 0!");
        }
    }

    public void toDecimal(int octalNumber) {
        int i = 0;
        int dec = 0;
        if (octalNumber > 0) {
            System.out.println("Восьмеричное число: " + octalNumber);
            while (octalNumber != 0) {
                dec = (int) (dec + (octalNumber % 10) * Math.pow(8, i++));
                octalNumber = octalNumber / 10;
            }
            System.out.println("равно десятичному числу: " + dec);
        } else {
            System.out.println("Число должно содержать только цифры 0-7!");
        }
    }
}
